﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Collections.Interfaces;

namespace Collections
{
    public class DynamicArray<T> : IDynamicArray<T>
    {
        private int _length;
        private T[] _array;
        private const int DefaultCapacity = 8;
        private const int DefaultLength = 0;
        public DynamicArray()
        {
            _array = new T[DefaultCapacity];
            _length = DefaultLength;
        }

        public DynamicArray(int capacity)
        {
            if (capacity < 0)
            {
                throw new ArgumentOutOfRangeException();
            }           
            _array = new T[capacity];
            _length = DefaultLength;
        }

        public DynamicArray(IEnumerable<T> items)
        {
            _array = new T[items.Count()];

            var a = items.GetEnumerator();

            var index = 0;

            while (a.MoveNext())
            {
                _array[index] = a.Current;

                index++;
            }
            _length = items.Count();
        }

        public IEnumerator<T> GetEnumerator()
        {
            var s = GetEnumerable<T>(_array);

            return s.GetEnumerator();
        }

        public IEnumerable<T> GetEnumerable<T>(T[] arr)
        {
            return arr;
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public T this[int index] //=> _array[index];           
        {
            get
            {
                if (index >= 0 && index < Length)
                {
                    return _array[index];
                }

                else 
                {
                    throw new IndexOutOfRangeException();
                }                    
            }
        }
        public int Length { get { return _length; } }
        public int Capacity { get { return _array.Length; } }

        public void Add(T item)
        {
            if (Length >= Capacity)
            {
                if (Capacity == 0)
                {
                    Array.Resize(ref _array, 1);
                }

                else
                {
                    Array.Resize(ref _array, Capacity * 2);
                }
            }

            _array[Length] = item;

            _length++;
        }

        public void AddRange(IEnumerable<T> items)
        {
            var input = items.ToArray();

            if (Length + input.Count() >= Capacity)
            {
                var newCapacitySize = GetCapacitySize(items.Count());

                Array.Resize(ref _array, newCapacitySize);
            }

            for (int i = Length; i < Length + input.Count(); i++)
            {
                _array[i] = input[i - Length];
            }

            _length = _length + input.Count();
        }

        public int GetCapacitySize(int amount)
        {
            amount = amount + Length; // changed Capacity on Length
            var capacity = Capacity;

            while (capacity < amount)
            {
                capacity = capacity * 2;
            }

            return capacity;
        }

        public void Insert(T item, int index)
        {
            if (index > Capacity || index < 0)
            {
                throw new IndexOutOfRangeException();
            }

            if (Length >= Capacity)
            {
                Array.Resize(ref _array, Capacity * 2);
            }

            Array.ConstrainedCopy(_array, index, _array, index + 1, Length - index);

            _array[index] = item;

            _length++;
        }

        public bool Remove(T item)
        {
            for (int i = 0; i < _array.Length; i++)
            {
                if (!_array[i].Equals(item))
                {
                    continue;
                }


                if (i < _array.Length)
                {
                    Array.ConstrainedCopy(_array, i + 1, _array, i, _array.Length - (i + 1));
                }
                else
                {
                    _array[i] = default(T);
                }

                _length--;

                return true;

            }
            return false;
        }
    }
}